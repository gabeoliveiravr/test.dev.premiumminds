﻿using System;

namespace Pokemon.Premium.Minds
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Diga ao Ash onde ir para capturar os Pokemons espalhados pelo mapa!\n\n " +
                "S = Sul\n\n N = Norte\n\n E = Este\n\n O = Oeste\n");

            string isValid, movement = Console.ReadLine();

            int steps = Coordinates(movement, out isValid);

            int[,] map = Catch(Map(steps), steps, isValid);

            int pokemons = Count(map);

            Console.WriteLine("\nO Ash capturou {1} Pokemons", steps, pokemons);
        }

        public static int Coordinates(string input, out string isValid)
        {
            string coordinates = "";
            int steps = 0;
            const char n = 'N', s = 'S', e = 'E', o = 'O';

            foreach (var x in input.ToUpper())
            {
                switch (x)
                {
                    case n:
                        steps++;
                        coordinates = string.Concat(coordinates + "n");
                        break;
                    case s:
                        steps++;
                        coordinates = string.Concat(coordinates + "s");
                        break;
                    case e:
                        steps++;
                        coordinates = string.Concat(coordinates + "e");
                        break;
                    case o:
                        steps++;
                        coordinates = string.Concat(coordinates + "o");
                        break;
                }
            }

            isValid = coordinates;
            return steps;
        }

        public static int[,] Map(int steps)
        {
            int size = steps * 2 + 1;
            int[,] map = new int[size, size];

            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    map[x, y] = 1;
                }
            }

            return map;
        }

        public static int[,] Catch(int[,] map, int step, string isValid)
        {
            int x = step, y = step;
            const char n = 'N', s = 'S', e = 'E', o = 'O';

            map[x, y] = 0;

            foreach (var k in isValid.ToUpper())
            {
                switch (k)
                {
                    case n:
                        y += 1;
                        map[x, y] = 0;
                        break;
                    case s:
                        y -= 1;
                        map[x, y] = 0;
                        break;
                    case e:
                        x += 1;
                        map[x, y] = 0;
                        break;
                    case o:
                        x -= 1;
                        map[x, y] = 0;
                        break;
                }

            }

            return map;
        }

        public static int Count(int[,] map)
        {
            int count = 0, size = (int)Math.Sqrt(map.Length);

            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    if (map[x, y] == 0)
                    {
                        count++;
                    }
                }
            }

            return count;
        }
    }
}
